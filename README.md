
# Drones Dashboard

A real-time dashboard for drones location

Project repo: https://gitlab.com/idoran/drones-dashboard

Detailes about the [server](server/README.md) and [client](client/README.md).

Built by Ido Ran (ido.ran@gmail.com).

# Facts

1. Drones fly within a single country
2. The drones communicate via cellular modem
3. Drones update their geo-location coordinates

# Missing definitions

1. How many drones are connected to the system at any given time?
1. How many drones exist in the system as whole?
1. Does each drone has it own unique ID?
1. What is active drone?
1. How often does a drone send location updates?
1. What unit of measure to display the speed?

# Assumptions

1. We'll assume everyone tell the truth - meaning we are not implementing any security checks but those can be added later.
1. We are not dealing with invalid data, for example from the drones. That can and should be add in the future.
1. The whole system (server and client) treat the drone id as string.
1. There are no more than few handred drones connected to the system at any point in time.

### Drones

1. Each drone has a unique ID setup and will send it when it is connecting to the central server. No two drones have the same unique ID (hence unique).
1. Drones keep on sending location updates every second or so.
1. Since the dashboard is only interested in the drones speed we'll simulate the drones updates by updating the speed directly without going through the calculation of location and time to get the speed. This can be change in the future when we'll work with location data but the end result will be the same.
1. Active drones are defined as drones that send location update in the last 10 seconds. We do not remove inactive drones but there is `lastUpdate` field which can be use to identify them.

### Server

1. The server will calculate the speed based on geo-location coordinates and time passed. It may not be the most accrurate but it will give good estimation of the drone's speed.
1. The server will expose a WebSocket endpoint for the drones to update their location. It should be more effecient for the drones to connect and identify once and then send only location updates as apposed to using HTTP and having to send the same data over and over. After WebSocket initial connection they have [virtually no overhead](https://stackoverflow.com/questions/26355077/overhead-of-idle-websockets).
1. The server will expose REST endpoint to update the drones data.
1. I didn't manage to implement the WebSocket endpoint for updating the drones data but I'll describe the strcture and function of it: The server will listen for WebSocket connection from the drones, once a connection is established the server will wait for a specific time to receive the identification of the drone connected, once this step is done the WebSocket will listen for location updates assuming the drone is sending data formatted using ProtoBuf messages. This will allow for both efficient and extensible protocol. For example, if in the future we'll want to also send the temperature measured by the drone or the height of the drone we can extend the ProtoBuf message and still support older version drones that do not send those fields.

### Client

1. The app show a single dashboard with list of drones
1. The list show the drone unqiue identifier and speed
1. The client-side will highlight drones that did not update their location for the last 10 seconds in red.
1. The client-side will highlight drones that their speed was zero for the last 10 seconds in yellow.

### Scaling

1. A single server can support a limited number of drones and web clients.
1. If the number of drones will grow a change to the system architecture will need to change by running more instances of the server app. This in turn will be depends on how the drones send updates. Assuming they are connecting using WebSocket we'll need to separate the WebSocket endpoint to a new application because we'll want to scale only that specific part of the application without the REST endpoints. The stateful behavior of the WebSockets means that once a drone has connect to a server it remain connected to that specific server. We might want to have a way to tell the drone when the system want it to connect to a different server to balance the load. This can be done by having the servers track the number of drones connected to each server and if to many are connected to a one server while other servers are less busy we'll send a ProtoBuf message to the drone requesting it to disconnect and connect to a specific different server.
1. Once we'll have the ability to run more Drone-Update-WebSocket servers we can either have those servers connect directly to the database to update it, or we can have them send REST requests to other servers. The later can allow for another level of indirection which can balance requests and also it will keep the WebSocket service clean from data-access which will allow easier changes once they will be required.


## Development

This project seeded from https://github.com/mrcoles/node-react-docker-compose.

```
docker-compose up
```

For development, the `server/` and `client/` directories have their own docker containers, which are configured via the `docker-compose.yml` file.

The client server is spun up at `localhost:3000` and it proxies internally to the server using the linked name as `server:8080`.

The local directories are mounted into the containers, so changes will reflect immediately. However, changes to package.json will likely need to a rebuild: `docker-compose down && docker-compose build && docker-compose up`.

### Notes

#### Adding new scss files

The `node-sass` watch feature does not notice new files. In order to get new files working, restart the client container:

```
docker-compose restart client
```

#### Installing npm dependencies

All changes to `node_modules` should happen *inside* the containers. Install any new dependencies by inside the container. You can do this via `docker-compose run`, but it’s easier to just upadte a running container and avoid having to rebuild everything:

```
docker-compose exec client
```

Then inside:

```
npm install --save <new_dependency>
```

## Production

```
docker-compose -f docker-compose.prod.yml up
```

For production, this uses the Dockerfile at the root of the repo. It creates a static build of the client React app and runs Express inside server, which handles both the API and serving of React files.

As a result, different code is executing to serve the React files, but all of the API calls should remain the same. The difference between development and production isn’t ideal, but it does offer the simplicity of having the entire app run in one server on one machine.

This is one of multiple ways a Node + React app could be setup, as suggested [here](https://daveceddia.com/create-react-app-express-production/):

*   __Keep them together__ - have Express serve both the API and React files
*   __Split them apart__ - have Express API on one machine and the React files on another (e.g., on S3 and use CORS to access the API)
*   __Put the API behind a proxy__ - use something like NGINX to proxy the Express API server and React static files separately

This project uses the “keep them together” approach. For better performance, you can set up a proxy (like Cloudflare) in between your server and the Internet to cache the static files. Or with some extra work you can fashion it to do either of the other two options.


## Notes

### Using docker compose

I have `comp` aliased to `docker-compose` on my computer.

Start via:

```
comp up

# or detached
comp up -d
```

Run a container of the server image via:

```
comp run server /bin/bash
```

Check status:

```
comp ps
```

Stop:

```
comp down
```

Run the production image:

```
comp -f docker-compose.prod.yml up
```

NOTE: if any dependencies change in package.json files, you probably will need to rebuild the container for the changes to appear, e.g.,

```
comp down
comp build
comp up
```


### Setup references

References for setting up a Node project with Docker and docker-compose:

*   https://nodejs.org/en/docs/guides/nodejs-docker-webapp/
*   https://blog.codeship.com/using-docker-compose-for-nodejs-development/
*   http://jdlm.info/articles/2016/03/06/lessons-building-node-app-docker.html

Express + React:

*   https://daveceddia.com/create-react-app-express-production/
*   http://ericsowell.com/blog/2017/5/16/create-react-app-and-express
*   https://medium.freecodecamp.org/how-to-make-create-react-app-work-with-a-node-backend-api-7c5c48acb1b0
*   https://medium.freecodecamp.org/how-to-host-a-website-on-s3-without-getting-lost-in-the-sea-e2b82aa6cd38

