# Drones Dashboard Web Client App

This is a dashboard web-app for flying drones.

The app show a list of drones, sorted by their unique identifier and their current (or last known) speed in meter/second.

The app highlight drones that are inactive in red and drones which are not moving in yellow.

## Technology

The app is a React app which uses `create-react-app` to eliminate the need for complex configuration.

If the scope grows beyond the abilities of `create-react-app` the app should be ejected and manage as standard Webpack application.

## Data management

The data of the app is managed inside the `App` component state. If the requirements will change it might be good idea to add data management library such as `redux`.

The data is requested once using REST API and updates are sent using WebSocket in real-time.
Both the API and WebSocket use the same data format.

The updates are sent by the server at most once a second so there is no need to optimize the client further.