import React, { Component } from 'react';
import Drone from './Drone';
import { isDroneInactive } from './drone-helper';
import './compiled/App.css';

/**
 * The root component of the app.
 */
class App extends Component {
  constructor() {
    super();

    this.handleFilterInactiveChange = this.handleFilterInactiveChange.bind(this);
    this.dronesFilter = this.dronesFilter.bind(this);

    this.state = {
      drones: []
    };
  }

  sortDronesList(drones) {
    drones.sort((a, b) => {
      if (a.id < b.id) return -1;
      if (a.id > b.id) return 1;
      return 0;
    });

    return drones;
  }

  registerToWebSocket(ws) {
    const that = this;

    // event emmited when connected
    ws.onopen = function () {
      console.log('websocket is connected ...')
      // sending a send event to websocket server
      ws.send('connected')
    }
    // event emmited when receiving message 
    ws.onmessage = function (ev) {
      console.log(ev);
      const updateDrones = JSON.parse(ev.data);

      const drones = that.state.drones;
      let newDrones = drones.slice();

      updateDrones.forEach((updateDrone) => {
        const currDroneIndex = drones.findIndex((drone) => drone.id === updateDrone.id);
        if (currDroneIndex === -1) {
          newDrones.push(updateDrone);
        } else {
          newDrones.splice(currDroneIndex, 1, updateDrone);
        }  
      });

      that.setState({ drones: that.sortDronesList(newDrones) });
    }
  }

  componentDidMount() {
    this.callApi()
      .then(res => this.setState({ drones: res }))
      .catch(console.error);

    // This is probably not the best solution.
    // What happens here is that in production and dev the server run
    // in different ports. This code essentionally tries to connect
    // to the production port and if fails it connects to the dev port.
    let ws;
    ws = new WebSocket('ws://localhost/dashboard');
    this.registerToWebSocket(ws);

    const that = this;
    ws.onerror = function (error) {
      ws = new WebSocket('ws://localhost:8080/dashboard');
      that.registerToWebSocket(ws);
    };

    
  }

  callApi = async () => {
    const resp = await fetch('/api/drones');

    let data = null;
    try {
      data = await resp.json();
    } catch (e) {
      console.err(`Invalid json\n${e}`);
    }

    if (resp.status !== 200) {
      throw Error(data ? data.message : 'No data');
    }

    return this.sortDronesList(data);
  };

  handleFilterInactiveChange(event) {
    const isInactiveFilter = event.target.checked;
    this.setState({ isInactiveFilter })
  }

  dronesFilter(drone) {
    const { isInactiveFilter } = this.state;
    if (!isInactiveFilter) {
      return true;
    } else {
      return !isDroneInactive(drone);
    }
  }

  render() {
    const { drones } = this.state;
    const dronesList = drones.filter(this.dronesFilter);
    const { isInactiveFilter } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Drones Dashboard</h1>
        </header>
        <div>
          <label>
            <input type="checkbox" onChange={this.handleFilterInactiveChange} />
            Filter inactive drones
            {isInactiveFilter ? (<span> ({drones.length - dronesList.length} hidden)</span>) : null}
          </label>
        </div>
        <div>
          {dronesList.length} Drones
        </div>
        <div>
          {dronesList.map(drone => (<Drone drone={drone} />))}
        </div>
      </div>
    );
  }
}

export default App;
