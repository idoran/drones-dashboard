import React, { Component } from 'react';
import { isDroneInactive, isDroneNotMoving } from './drone-helper';

/**
 * Represent a single drone in the dashboard view.
 */
export default class Drone extends Component {

  render() {
    const { drone } = this.props;

    const alertBufferInSeconds = 10;
    let highlight; 
    if (isDroneInactive(drone)) {
      highlight = 'no-updates';
    } else if (isDroneNotMoving(drone)) {
      highlight = 'no-moving-highlight';
    }
    return (
      <div key={`drone-${drone.id}`} className={highlight}>{drone.id} - {drone.speed}</div>
    )
  }
}
