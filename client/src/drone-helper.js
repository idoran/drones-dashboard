const alertBufferInSeconds = 10;

/**
 * Get indication if the drone is inactive.
 * @param {*} drone 
 */
export function isDroneInactive(drone) {
  const lastUpdatePlusBuffer = new Date(1000 * (drone.lastUpdate + alertBufferInSeconds));
  return (lastUpdatePlusBuffer < Date.now())
}

/**
 * Get indication if the drone is not moving.
 * @param {*} drone 
 */
export function isDroneNotMoving(drone) {
  const lastSpeedUpdatePlusBuffer = new Date(1000 * (drone.lastSpeedUpdate + alertBufferInSeconds));
  return ((drone.speed === 0 && lastSpeedUpdatePlusBuffer < Date.now()))
}