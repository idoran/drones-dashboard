const { expect } = require('chai');
const { calculateSpeedFromTwoLocations, distVincenty } = require('../src/geo-location');

function nowEpoch() {
  return new Date().getDate() / 1000;
}

describe('geo-location', () => {
  it('distance from latLong', (done) => {
    const distnaceInMeters = distVincenty(32.033534, 34.818958, 32.032834, 34.816855);
    expect(Math.round(distnaceInMeters)).to.equal(213);
    done();
  });

  it('speed from latLong and time', (done) => {
    const now = nowEpoch();
    const speedKph = calculateSpeedFromTwoLocations(
      [32.033534, 34.818958], [32.032834, 34.816855],
      now - 10, now,
    );

    expect(Math.round(speedKph)).to.equal(77);
    done();
  });
});
