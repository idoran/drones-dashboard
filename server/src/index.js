const express = require('express');
const http = require('http');
const WebSocket = require('ws');
const path = require('path');
const bodyParser = require('body-parser');

const { handleDrones, handleUpdateDrone } = require('./drones');
const dronesWSHandler = require('./dronesws');

// Constants
const PORT = process.env.PORT || 8080;
const HOST = '0.0.0.0';

const CLIENT_BUILD_PATH = path.join(__dirname, '../../client/build');

// App
const app = express();

// initialize a simple http server
const server = http.createServer(app);

// initialize the WebSocket server instance
const wss = new WebSocket.Server({ server, path: '/dashboard' });

// parsing JSON request body
app.use(bodyParser.json());

// Static files
app.use(express.static(CLIENT_BUILD_PATH));

// API
app.get('/api/drones', handleDrones);

app.post('/api/drones', handleUpdateDrone);

// All remaining requests return the React app, so it can handle routing.
app.get('*', (request, response) => {
  response.sendFile(path.join(CLIENT_BUILD_PATH, 'index.html'));
});

dronesWSHandler(wss);

server.listen(PORT, HOST);

// eslint-disable-next-line no-console
console.log(`Running on http://${HOST}:${PORT}`);
