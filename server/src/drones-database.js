const { nowEpoch } = require('./time-helper');

const initEpoch = nowEpoch();

let dronesData = {};

if (process.env.NODE_ENV !== 'production') {
  dronesData = {
    A1: {
      latLong: [12.2231, 2323.22222],
      speed: 25,
      lastUpdate: initEpoch,
    },
    A2: {
      latLong: [16.2231, 332.2312],
      speed: 0,
      lastUpdate: initEpoch - (14 * 60),
    },
    A3: {
      latLong: [982.21, 212.3322],
      speed: 15,
      lastUpdate: initEpoch - (14 * 60),
    },
  };
}

const notifyClients = [];

/**
 * Get all the drones in the database.
 */
function get() {
  return dronesData;
}

/**
 * Get a single drone by Id.
 * @param {string} droneId The droneId to get
 */
function getById(droneId) {
  return dronesData[droneId];
}

/**
 * Notify that the specific drone was changed.
 * @param {string} id The droneId that changed.
 */
function notify(id) {
  notifyClients.forEach((client) => {
    client(id, dronesData[id]);
  });
}

/**
 * Update a specific drone data.
 * @param {string} id droneId
 * @param {object} newData Updated drone data.
 */
function set(id, newData) {
  const currData = dronesData[id];
  const newEpoch = nowEpoch();
  const enrichedData = {
    ...newData,
    lastUpdate: newEpoch,
    lastSpeedUpdate: (!currData || currData.speed !== newData.speed)
      ? newEpoch : currData.lastSpeedUpdate,
  };

  dronesData[id] = enrichedData;
  notify(id, enrichedData);
}

/**
 * Register a callback to be called when the database changes.
 * @param {function} callback Callback function
 */
function register(callback) {
  notifyClients.push(callback);
}

module.exports = {
  get,
  getById,
  set,
  register,
};
