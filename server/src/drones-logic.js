const dronesDatabase = require('./drones-database');
const { calculateSpeedFromTwoLocations } = require('./geo-location');
const { nowEpoch } = require('./time-helper');

function updateDrone(droneId, latLong) {
  const currDroneData = dronesDatabase.getById(droneId);
  if (!currDroneData) {
    // This is the first update of the drone
    dronesDatabase.set(droneId, {
      latLong,
      speed: 0,
    });
  } else {
    // Update speed based on last location and time
    const { latLong: lastLatLong, lastUpdate } = currDroneData;
    const speed = calculateSpeedFromTwoLocations(lastLatLong, latLong, lastUpdate, nowEpoch());
    dronesDatabase.set(droneId, {
      latLong,
      speed,
    });
  }
}

module.exports = {
  updateDrone,
};
