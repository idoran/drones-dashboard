const dronesDatabase = require('./drones-database');
const droneApiHelper = require('./drone-api-helper');
const { updateDrone } = require('./drones-logic');

let fakeIndex = 0;

function handleDrones(req, res) {
  const dronesData = dronesDatabase.get();
  res.set('Content-Type', 'application/json');
  const data = Object.keys(dronesData).map(id => (
    droneApiHelper.buildDroneResponse(id, dronesData[id])));
  res.send(JSON.stringify(data, null, 2));
}

function handleUpdateDrone(req, res) {
  const { droneId, latLong } = req.body;

  if (!droneId || !latLong) {
    res.status(400).send('invalid body parameters');
  } else {
    const result = updateDrone(droneId, latLong);
    res.send(result);
  }
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function fakeDronesData() {
  fakeIndex += 1;

  dronesDatabase.set('A1',
    {
      latLong: [12.2231, 2323.22222],
      speed: getRandomInt(30),
    });

  if (fakeIndex < 10) {
    dronesDatabase.set('A2',
      {
        latLong: [12.2231, 2323.22222],
        speed: getRandomInt(30),
      });
  }

  const drone3Speed = getRandomInt(30);
  dronesDatabase.set('A3',
    {
      latLong: [12.2231, 2323.22222],
      speed: drone3Speed > 29 ? drone3Speed : 0,
    });

  for (let index = 4; index < 30; index += 1) {
    dronesDatabase.set(`A${index}`,
      {
        latLong: [12.2231, 2323.22222],
        speed: getRandomInt(30),
      });
  }
}

if (process.env.NODE_ENV !== 'production') {
  setInterval(fakeDronesData, 500);
}

module.exports = {
  handleDrones,
  handleUpdateDrone,
};
