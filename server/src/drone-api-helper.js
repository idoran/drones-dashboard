/**
 * Build an object to send to the client which represents a
 * single drone data.
 */
function buildDroneResponse(id, droneData) {
  return {
    ...droneData,
    id,
  };
}

module.exports = {
  buildDroneResponse,
};
