const dronesDatabase = require('./drones-database');
const droneApiHelper = require('./drone-api-helper');

const clients = [];

const updatedDroneIds = new Set();

function messageFromClient(client, message) {
  // client.send(`Hello, you sent -> ${message}`);
}

function dronesWSHandler(wss) {
  wss.on('connection', (ws) => {
    clients.push(ws);

    // connection is up, let's add a simple simple event
    const me = ws;
    ws.on('message', (message) => {
      messageFromClient(me, message);
    });

    // send immediatly a feedback to the incoming connection
    // ws.send('Hi there, I am a WebSocket server');
  });
}

function publishUpdates() {
  // Map all updated drones data to a single array.
  const updates = [...updatedDroneIds].map(id => (
    droneApiHelper.buildDroneResponse(id, dronesDatabase.get()[id])
  ));

  // Clear the updted drones ids for next batch.
  updatedDroneIds.clear();

  clients.forEach((client) => {
    const msg = JSON.stringify(updates);

    try {
      client.send(msg);
    } catch (ex) {
      console.log('removing disconnected client...');
      clients.splice(clients.indexOf(client), 1);
    }
  });
}

dronesDatabase.register((id) => {
  if (updatedDroneIds.size === 0) {
    setTimeout(publishUpdates, 1000);
  }
  updatedDroneIds.add(id);
});

module.exports = dronesWSHandler;
