/**
 * Get the current time as epoch number.
 */
function nowEpoch() {
  return new Date().getTime() / 1000;
}

module.exports = {
  nowEpoch,
};
