# Drone Dashboard Server

This folder contain the drone dashboard server.

The server is built with NodeJS and uses both REST over HTTPS and WebSocket interfaces.

## REST API

### api/drones

This endpoint returns the full list of drones in JSON format.

Example response
```
[
  {
    latLong: [12.2231, 2323.22222],
    speed: 12,
    lastUpdate: 123123,
    lastSpeedUpdate: 123123
  }
]
```

## WebSocket API

### /dashboard

This endpoint allows client to connect and receive real-time update as the drones database changes.
It uses the exact same format as `api/drones` API.

### /drones

This endpoint allows drones to connect and after authentication send location updates in efficient way.

The drones will send location update using decimal coornidates every second or so.

## Test

To run all the tests use `npm test`.  
To run test and monitor files use `npm run auto-test`.

## Lint

The project is configured with ESLint. Using an IDE which supports ESLint is recommended (such as Visual Studio Code).  
To run lint manually use `npm run lint`.