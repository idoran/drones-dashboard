# Fake Drones

This project simulate a fleat of drones which send location updates to the Drone Dashboard.

## How to use?

Run `npm start` and answer the questions prompted:
1. Port - the port of the server. Usually `8080` for dev and `80` to production.
1. How many drones - the number of drones to simulate.
1. How many Inactive - this is the modulo value of each nth drone that will be inactive.
1. How many not moving - this is a modulo value of each nth drone that will not move.

Both inactive and moving use random to simulate drones that usually (90% of the time) are inactive or not moving.