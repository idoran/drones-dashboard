const unirest = require('unirest');
const prompt = require('prompt');

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function getRandomFraction() {
  return Math.random() * 0.0001;
}

prompt.get(['port', 'howManyDrones', 'howManyInactive', 'howManyNotMoving'], (err, result) => {
  const {
    port,
    howManyDrones,
    howManyInactive,
    howManyNotMoving,
  } = result;

  const notMovingLatLong = [31.2231, 34.2221];

  function fakeOneDrone() {
    for (let droneIndex = 0; droneIndex < howManyDrones; droneIndex += 1) {

      let updateDrone = true;
      let latLong = [31.893111 + getRandomFraction(), 34.780574 + getRandomFraction()];

      if (howManyInactive > 0 && droneIndex % howManyInactive) {
        updateDrone = getRandomInt(100) > 90;
      } else if (howManyNotMoving > 0 && droneIndex % howManyNotMoving) {
        if (getRandomInt(100) < 90) {
          latLong = notMovingLatLong.concat();
        }
      }

      if (updateDrone) {
        unirest.post(`http://localhost:${port}/api/drones`)
          .headers({ Accept: 'application/json', 'Content-Type': 'application/json' })
          .send({ droneId: `B${droneIndex}`, latLong })
          .end();
      }
    }
  }

  setInterval(fakeOneDrone, 1000);
});
